libipc-pubsub-perl (0.29-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 14:07:16 +0000

libipc-pubsub-perl (0.29-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Refer to specific version of license GPL-1+.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 14:49:20 +0100

libipc-pubsub-perl (0.29-2) unstable; urgency=medium

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove AGOSTINI Yves from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Update to dpkg source format 3.0 (quilt)
  * Update to debhelper compat level 10 and tiny debian/rules
  * Add myself to Uploaders
  * Fix DEP-5 debian/copyright syntax
  * Add build dependencies and run time suggestions on the alternative
    backends based on Cache::Memcached and Jifty::DBI
  * Update to Standards-Version 4.1.3

 -- Niko Tyni <ntyni@debian.org>  Mon, 15 Jan 2018 21:50:38 +0200

libipc-pubsub-perl (0.29-1) unstable; urgency=low

  [ AGOSTINI Yves ]
  * new 0.29 upstream release (0.28 was not packaged for debian)
  * bump Standards-Version to 3.8.0, no change
  * update copyright to proposal format rev 196
  * add copyright for M::I inc directory
  * add libdbm-deep-perl dep in control

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Mon, 15 Dec 2008 11:27:34 +0100

libipc-pubsub-perl (0.27-1) unstable; urgency=low

  * Initial Release (Closes: #476743)

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Fri, 18 Apr 2008 23:33:58 +0200
